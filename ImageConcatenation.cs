﻿using System.Drawing;
using System.Drawing.Imaging;
using ImageProcessingUtils;

namespace Assignment2
{
   abstract class ImageConcatenation : IDisposable
   {
      private Bitmap _bitmap;
      private bool _disposed ;

      public ImageConcatenation(Bitmap bitmap)
      {
         this._bitmap = bitmap;
      }

      public void Dispose()
      {
         Dispose(true);
         GC.SuppressFinalize(this);
      }

      protected virtual void Dispose(bool disposing)
      {
         if (!_disposed)
         {
            if (disposing)
            {
               _bitmap.Dispose();
            }

            _disposed = true;
         }
      }

      ~ImageConcatenation()
      {
         Dispose(false);
      }
      
      static void Main(string[] args)
      {
         string directory = Path.GetDirectoryName(Directory.GetCurrentDirectory());
         string image1Path = Path.Combine(directory, "image1.jpg");
         string image2Path = Path.Combine(directory, "image2.jpg");

         Bitmap image1 = new Bitmap(image1Path);
         Bitmap image2 = new Bitmap(image2Path);

         image1 = ImageProcessing.ConvertToGray(image1);
         image2 = ImageProcessing.ConvertToGray(image2);

         Bitmap concatenateHorizontally = ConcatenateImages(image1, image2,true);
         concatenateHorizontally.Save("Horizontal.bmp", ImageFormat.Bmp);

         Bitmap concatenateVertically = ConcatenateImages(image1, image2, false);
         concatenateVertically.Save("Vertical.bmp", ImageFormat.Bmp);
         
         Console.WriteLine("Images concatenated successfully.");
         
         image1.Dispose();
         image2.Dispose();
         concatenateHorizontally.Dispose();
         concatenateVertically.Dispose();
      }

      static Bitmap ConcatenateImages(Bitmap image1, Bitmap image2, bool horizontally)
      {
         int width1 = image1.Width;
         int height1 = image1.Height;

         int width2 = image2.Width;
         int height2 = image2.Height;

         int maxWidth = Math.Max(width1, width2);
         int maxHeight = Math.Max(height1, height2);

         Bitmap resultImage = new Bitmap(horizontally ? width1 + width2 : maxWidth,
            horizontally ? maxHeight : height1 + height2,
            PixelFormat.Format8bppIndexed);

         ColorPalette palette = resultImage.Palette;
         for (int i = 0; i < 256; i++)
         {
            palette.Entries[i] = Color.FromArgb(i, i, i);
         }

         resultImage.Palette = palette;

         unsafe
         {
            BitmapData image1Data = image1.LockBits(new Rectangle(0, 0, width1, height1), ImageLockMode.ReadOnly,
               PixelFormat.Format8bppIndexed);
            byte* image1Scan0 = (byte*)image1Data.Scan0;
            int image1Stride = image1Data.Stride;
            
            BitmapData image2Data = image2.LockBits(new Rectangle(0, 0, width2, height2), ImageLockMode.ReadOnly,
               PixelFormat.Format8bppIndexed);
            byte* image2Scan0 = (byte*)image2Data.Scan0;
            int image2Stride = image2Data.Stride;
            
            BitmapData resultData = resultImage.LockBits(new Rectangle(0, 0, resultImage.Width, resultImage.Height),
               ImageLockMode.WriteOnly, PixelFormat.Format8bppIndexed);
            byte* resultScan0 = (byte*)resultData.Scan0;
            int resultStride = resultData.Stride;

            for (int y = 0; y < height1; y++)
            {
               byte* resultPointer = resultScan0 + y * resultStride;
               byte* image1Pointer = image1Scan0 + y * image1Stride;

               for (int x = 0; x < width1; x++)
               {
                  byte pixel = image1Pointer[x];
                  resultPointer[x] = pixel;
               }
            }
            if (horizontally)
            {
               for (int y = 0; y < maxHeight; y++)
               {
                  byte* resultPointer = resultScan0 + y * resultStride;
                  byte* image2Pointer = image2Scan0 + y * image2Stride;

                  for (int x = 0; x < width2; x++)
                  {
                     byte pixel = image2Pointer[x];
                     resultPointer[width1 + x] = pixel;
                  }
               }
            }
            else
            {
               for (int y = 0; y < height2; y++)
               {
                  byte* resultPointer = resultScan0 + (y + height1) * resultStride;
                  byte* image2Pointer = image2Scan0 + y * image2Stride;

                  for (int x = 0; x < width2; x++)
                  {
                     byte pixel = image2Pointer[x];
                     resultPointer[x] = pixel;
                  }
               }
            }
            image2.UnlockBits(image2Data);
            image1.UnlockBits(image1Data);
            resultImage.UnlockBits(resultData);
         }
         return resultImage;
      }
   }
}